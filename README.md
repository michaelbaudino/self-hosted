# self-hosted

This is a collection of self-hosted services I use for my private use.

## Roadmap

* [ ] [PhotoPrism](https://photoprism.app/) or [Photonix](https://photonix.org)
* [ ] [MiniFlux](https://miniflux.app)
* [ ] [Wallabag](https://wallabag.org)
* [ ] [RSSHub](https://rsshub.app)
* [ ] [WatchTower](https://containrrr.dev/watchtower)
